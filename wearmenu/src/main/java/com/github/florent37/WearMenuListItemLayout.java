package com.github.florent37;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.Image;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;
public class WearMenuListItemLayout extends BaseItemProvider {
    final List<WearMenuListViewAdapterViewHolder> list;
    final Context context;
    public WearMenuListItemLayout(List<WearMenuListViewAdapterViewHolder> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }
    @Override
    public Object getItem(int position) {
        if (list != null && position >= 0 && position < list.size()) {
            return list.get(position);
        }
        return null;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_wearmenu_list_element, null, false);
        } else {
            cpt = convertComponent;
        }
        WearMenuListViewAdapterViewHolder sampleItem = list.get(position);
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_text);
        Image image = (Image) cpt.findComponentById(ResourceTable.Id_imageButton);
        ShapeElement shapeElement = new ShapeElement();
        if (!sampleItem.isEmpty()) {
            text.setText(sampleItem.getName());
            image.setPixelMap(sampleItem.getImage());
            if (sampleItem.isSelected()) {
                shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLUE.getValue()));
            } else {
                shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.GRAY.getValue()));
            }
            shapeElement.setShape(ShapeElement.OVAL);
            image.setBackground(shapeElement);
        } else {
            text.setText(null);
            shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
            image.setBackground(shapeElement);
        }
        return cpt;
    }

}