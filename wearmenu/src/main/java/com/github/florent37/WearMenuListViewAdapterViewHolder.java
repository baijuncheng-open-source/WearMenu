package com.github.florent37;

public class WearMenuListViewAdapterViewHolder {
    String itemName;
    int image;
    boolean selected;
    boolean empty;

    public WearMenuListViewAdapterViewHolder(String itemName, int image) {
        this.itemName = itemName;
        this.image = image;
    }
    public String getName() {
        return itemName;
    }

    public void setName(String itemName) {
        this.itemName = itemName;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }
}
