package com.github.florent37;

import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Optional;

public class WearMenu implements Component.TouchEventListener {

    private static float startX;
    private static float startY;
    private static float endX;
    private static float endY;
    private static boolean isShow = false;


    public static void deal(DependentLayout wear_menu, ListContainer pager,DependentLayout pagerFather) {
        wear_menu.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent event) {
                int index = event.getIndex();
                float x = event.getPointerScreenPosition(index).getX();
                float y = event.getPointerScreenPosition(index).getY();
                switch (event.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        startX = x;
                        startY = y;
                        break;
                    case TouchEvent.POINT_MOVE:

                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                        endX = x;
                        endY = y;
                        if ((endX - startX) > 50 && (startY - endY) > 20) {
                            if (!isShow) {
                                onShowAnimator(pagerFather,pager);
                            }
                        } else if ((startX - endX) > 50 && (endY - startY) > 20) {
                            if (isShow) {
                                onUnShowAnimator(pagerFather,pager);
                            }
                        }
                        break;
                }
                return true;
            }
        });
    }
    public static void onShowAnimator(DependentLayout pagerFather,ListContainer pager) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(pagerFather.getContext());
        Point pt = new Point();
        display.get().getSize(pt);
        int pointY = pt.getPointYToInt();
        int pointX = pt.getPointXToInt();
        AnimatorProperty animatorProperty1 = pagerFather.createAnimatorProperty()
                .moveFromX(-pointX / 2f).moveToX(0)
                .moveFromY(pointY / 2f).moveToY(-10)
                .scaleXFrom(0).scaleX(1)
                .scaleYFrom(0).scaleY(1)
                .setDuration(500);
        animatorProperty1.start();
        pager.setScrollbarOverlapEnabled(false);
        pagerFather.setVisibility(Component.VISIBLE);
        pager.setVisibility(Component.VISIBLE);
        isShow = true;
    }

    public static void onUnShowAnimator(DependentLayout pagerFather,ListContainer pager) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(pagerFather.getContext());
        Point pt = new Point();
        display.get().getSize(pt);
        int pointY = pt.getPointYToInt();
        AnimatorProperty animatorProperty1 = pagerFather.createAnimatorProperty()
                .moveFromX(0).moveToX(-pointY * 2)
                .moveFromY(0).moveToY(pointY * 2)
                .scaleXFrom(1).scaleX(0)
                .scaleYFrom(1).scaleY(0)
                .setDuration(800);
        animatorProperty1.start();
        isShow = false;
    }
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        return false;
    }
}
