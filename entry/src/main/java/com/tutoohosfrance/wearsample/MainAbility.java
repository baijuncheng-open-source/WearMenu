package com.tutoohosfrance.wearsample;

import com.github.florent37.WearMenuListViewAdapterViewHolder;
import com.github.florent37.WearMenuListItemLayout;
import com.github.florent37.WearMenu;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

public class MainAbility extends Ability {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "MainAbility");

    private ListContainer pager;
    DependentLayout wear_menu;
    DependentLayout pagerFather;
    List<WearMenuListViewAdapterViewHolder> elementList = new ArrayList<>();
    private WearMenuListItemLayout sampleItemProvider;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        pager = (ListContainer) findComponentById(ResourceTable.Id_pager);
        pagerFather = (DependentLayout) findComponentById(ResourceTable.Id_pagerFather);
        elementList = getData();
        if (sampleItemProvider == null) {
            sampleItemProvider = new WearMenuListItemLayout(elementList,this);
        }
        pager.setItemProvider(sampleItemProvider);
        pager.setReboundEffect(false);
        wear_menu = (DependentLayout) findComponentById(ResourceTable.Id_wear_menu);
        WearMenu.deal(wear_menu,pager,pagerFather);
        pager.addScrolledListener(new Component.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
                for (WearMenuListViewAdapterViewHolder wearMenuListViewAdapterViewHolder : elementList) {
                    wearMenuListViewAdapterViewHolder.setSelected(false);
                }
                int centerFocusablePosition = pager.getCenterFocusablePosition();
                if (centerFocusablePosition<elementList.size()){
                    elementList.get(centerFocusablePosition).setSelected(true);
                }
                sampleItemProvider.notifyDataChanged();
            }
        });
        pager.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                if (i > 0 && i < elementList.size() - 1) {
//                    new ToastDialog(MainAbility.this).setOffset(20,150).setContentText(String.format("Selected Title %d", i)).show();
                }
            }
        });

        pager.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                int a = pager.getCenterFocusablePosition();
                switch (touchEvent.getAction()) {
                    case TouchEvent.POINT_MOVE:
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                        break;
                }
                return true;
            }
        });
        pager.setShaderColor(Color.WHITE);
    }

    private ArrayList<WearMenuListViewAdapterViewHolder> getData() {
        ArrayList<WearMenuListViewAdapterViewHolder> list = new ArrayList<>();
        int[] img = new int[]{
                ResourceTable.Media_ic_car,
                ResourceTable.Media_ic_car,
                ResourceTable.Media_ic_notif,
                ResourceTable.Media_ic_picture,
                ResourceTable.Media_ic_speak,
                ResourceTable.Media_ic_speak
        };
        for (int i = 0; i < img.length; i++) {
            list.add(new WearMenuListViewAdapterViewHolder("Title " + i,img[i]));
        }
        list.get(0).setEmpty(true);
        list.get(1).setSelected(true);
        list.get(list.size() - 1).setEmpty(true);
        return list;
    }

}
