# WearMenu

**the library for WearMenu swipe to the upper right to call out the menu,
 swipe up and down to browse the selection, click on the menu to select the menu, 
 slide to the lower left to hide the menu.**

WearMenu available are:
>1.Call up the menu
>2.Hide menu
>3.Select item


## Screenshot
![输入图片说明](https://images.gitee.com/uploads/images/2021/0708/093019_603eb3d1_8230582.gif "wearmenu.gif")

## How to include it in your project:

### Solution 1: Local HAR package integration

Add the HAR(WearMenu-debug.har) package to the lib folder

Add the following code to gradle of Entry

implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

### Solution 2: remote maven repo integration

Add the following code in the entry's gradle

implementation 'com.gitee.baijuncheng-open-source:WearMenu:1.0.0'


## Sample usage

```
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        pager = (ListContainer) findComponentById(ResourceTable.Id_pager);
        pagerFather = (DependentLayout) findComponentById(ResourceTable.Id_pagerFather);
        elementList = getData();
        if (sampleItemProvider == null) {
            sampleItemProvider = new WearMenuListItemLayout(elementList,this);
        }
        pager.setItemProvider(sampleItemProvider);
        pager.setReboundEffect(false);
        wear_menu = (DependentLayout) findComponentById(ResourceTable.Id_wear_menu);
        WearMenu.deal(wear_menu,pager,pagerFather);
        pager.addScrolledListener(new Component.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
                for (WearMenuListViewAdapterViewHolder wearMenuListViewAdapterViewHolder : elementList) {
                    wearMenuListViewAdapterViewHolder.setSelected(false);
                }
                int centerFocusablePosition = pager.getCenterFocusablePosition();
                if (centerFocusablePosition<elementList.size()){
                    elementList.get(centerFocusablePosition).setSelected(true);
                }
                sampleItemProvider.notifyDataChanged();
            }
        });
        pager.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                if (i > 0 && i < elementList.size() - 1) {
//                    new ToastDialog(MainAbility.this).setOffset(20,150).setContentText(String.format("Selected Title %d", i)).show();
                }
            }
        });

        pager.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                int a = pager.getCenterFocusablePosition();
                switch (touchEvent.getAction()) {
                    case TouchEvent.POINT_MOVE:
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                        break;
                }
                return true;
            }
        });
        pager.setShaderColor(Color.WHITE);
    }

    private ArrayList<WearMenuListViewAdapterViewHolder> getData() {
        ArrayList<WearMenuListViewAdapterViewHolder> list = new ArrayList<>();
        int[] img = new int[]{
                ResourceTable.Media_ic_car,
                ResourceTable.Media_ic_car,
                ResourceTable.Media_ic_notif,
                ResourceTable.Media_ic_picture,
                ResourceTable.Media_ic_speak,
                ResourceTable.Media_ic_speak
        };
        for (int i = 0; i < img.length; i++) {
            list.add(new WearMenuListViewAdapterViewHolder("Title " + i,img[i]));
        }
        list.get(0).setEmpty(true);
        list.get(1).setSelected(true);
        list.get(list.size() - 1).setEmpty(true);
        return list;
    }

```
## Changelog
## [1.0.0] - June , 2021

* 增加WearMenu demo;

## License
```
Copyright 2015 florent37, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
>Todo: Add documentation.